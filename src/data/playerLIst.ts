export const playerList = [{
  name: 'Sisinio',
  position: 'FW'
},
  {
    name: 'Дидье Я Конан',
    position: 'MF'
  },
  {
    name: 'Иван Лень',
    position: 'DF'
  },
  {
    name: 'Стив Манданда',
    position: 'GK'
  },
  {
    name: 'Павел Ребенок',
    position: 'MF'
  },
  {
    name: 'Бенуа Педретти',
    position: 'MF'
  },
  {
    name: 'Марьян Гад',
    position: 'DF'
  },
  {
    name: 'Kaka',
    position: 'MF'
  },
  {
    name: 'Стефано Окака Чука',
    position: 'MF'
  },
  {
    name: 'Самир Насри',
    position: 'FW'
  },
  {
    name: 'Георгий Какалов',
    position: 'MF'
  },
  {
    name: 'Абдулла Дурак',
    position: 'MF'
  },
  {
    name: 'Маркус Даун',
    position: 'FW'
  },
  {
    name: 'Мариуш Попа',
    position: 'GK'
  },
  {
    name: 'Адриан Пуканыч',
    position: 'FW'
  },
  {
    name: 'Хассан Йебда',
    position: 'MF'
  },
  {
    name: 'Рафал Пивко',
    position: 'MF',
  },
  {
    name: 'Захари Сираков',
    position: 'FW',
  },
  {
    name: 'Ribko',
    position: 'MF'
  },
  {
    name: 'Pukki',
    position: 'MF',
  },
  {
    name: 'Ananidze',
    position: 'MF',
  },
  {
    name: 'Dzuba',
    position: 'FW'
  },
  {
    name: 'F. Herpoel',
    position: 'GK',
  },
  {
    name: 'Sukasyan',
    position: 'GK',
  },
  {
    name: 'J. Pajuelo',
    position: 'DF'
  },
  {
    name: 'J. Huyveld',
    position: 'DF'
  },
  {
    name: 'P. Mraz',
    position: 'DF'
  },
  {
    name: 'Pjanic',
    position: 'MF'
  },
];


// 15. Евгений Коноплянка
// Украинский полузащитник «Днепра» Евгений Коноплянка одной фамилией вызывает непроизвольную улыбку, перемигивания и характерные жесты у многих болельщиков, хотя его фамилия по сути всего лишь птичка. С тем же успехом можно хихикать над Андреем Воробьем из «Металлиста», но хихикают над Коноплянкой.
//
// 14. Павел Чмовш
// Защитник нидерландского клуба НЕК Павел Чмовш борется за место в сборной Чехии и сердцах болельщиков, но российские сердца он покоряет одной фамилией.
//
// 8. Маурицио Хуельмо
// Игрок бразильского любительского клуба «АА Кальденсе» Маурицио Хуельмо даже не подозревает, что является не последней личностью в российской Интернет среде из-за своей оригинальной и не совсем благозвучной фамилии.
//
// 7. Абу Огого
// Воспитанник лондонского «Арсенала» Абу Огого на данный момент выступает в «Лиге Два», по сути четвертом английском дивизионе, за клуб с не менее интересным названием «Дагенем энд Редбридж» и заставляет восхищаться своей фамилией российских болельщиков. Хотя нашему люду куда привычнее фамилия Мариуша Йопа для выражения восхищения.
//
//   Абу Огого
//
// 6. Золтан Чурка
// Не догнал пятерку лидеров, заскользив по газону в лаковых туфлях, игрок эстонского клуба «Таммека» Золтан Чурка. Талантливый венгр стал бы первоклассным объектом шуток и «троллинга» в случае переезда в Россию, но пока данная участь уготована соседям из Эстонии.
//
// 5. Ахмед Ахахауи
// Форвард нидерландского клуба «Волендам» из второго по сути дивизиона Ахмед Ахахауи, более органично смотрелся бы в клубе «Химнасия де Хухуй» из провинции Хухуй. В России канонически правильная транскрипция и транслитерация города и провинции Хухуй часто заменяется на «Жужуй» с целью благозвучия. Однако, настоящий футбольные болельщики наверняка знают, что Хухуй, он и в России Хухуй.
//
//
// 3. Франтишек Анус
// Тройку лидеров открывает игрок из футбольного клуба «Тринец» второго чешского дивизиона Франтишек Анус, который своей фамилией полностью характеризует свое положение в мировой футбольной иерархии. Хотя для российских болельщиков совсем неважен клуб Франтишека, им достаточно его фамилии.
//
// 2. Хуан Пахуэло
// Занимающий почетное второе место, перуанский защитник Хуан Пахуэло всю карьеру провел в достаточно посредственных футбольных клубах, а ныне защищает цвета более чем скромной команды «Хосе Гальвес» из Перу. Однако, игроку с такой фамилией должны быть все равно на такие мелочи как имидж команды, турниры и кубки, ведь он настоящий Пахуэло.
//
// 1. Фредерик Херпоел
// Мы подошли к тому, что бы назвать победителя нашего сегодняшнего рейтинга самых смешных имен и фамилий футболистов. Им становится бельгийский вратарь Фредерик Херпоел, который совсем недавно закончил карьеру игрока. В России существует два варианта фамилии бывшего голкипера сборной Бельгии: Херпоэль и Херпоел. Какой из них более правильный выбирать вам, а мы единогласным решением остановились на втором варианте. Таким образом, импровизированная победа достается Фредерику Херпоелу.
//   Читать полностью: https://www.pressball.by/articles/others/digest/84002
