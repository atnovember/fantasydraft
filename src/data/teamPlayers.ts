
export const teamPlayers = [
  {
    position: 'FW',
    positionOnField: 'FW',
    name: null,
  },
  {
    position: 'FW',
    positionOnField: 'FWL',
    name: 'Berthwaite',
  },
  {
    position: 'FW',
    positionOnField: 'FWR',
    name: null,
  },
  {
    position: 'MF',
    positionOnField: 'LMF',
    name: null,
  },
  {
    position: 'MF',
    positionOnField: 'LCMF',
    name: null,
  },
  {
    position: 'MF',
    positionOnField: 'RCMF',
    name: null,
  },
  {
    position: 'MF',
    positionOnField: 'RMF',
    name: null,
  },
  {
    position: 'DF',
    positionOnField: 'DF',
    name: null,
  },
  {
    position: 'DF',
    positionOnField: 'RDF',
    name: null,
  },
  {
    position: 'DF',
    positionOnField: 'LDF',
    name: null,
  },
  {
    position: 'GK',
    positionOnField: 'GK',
    name: 'Krivoruchko',
  }
]
