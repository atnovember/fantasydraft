// @ts-nocheck
import React from 'react';
import {useDrop} from 'react-dnd';
import {PlayerOnPosition} from "./PlayerOnPosition";

// Dustbin analog
/** КАРТОЧКА ИГРОКА НА ПОЛЕ */
export default function PlayerPosition(
  {positionShortName, name, positionOnField, acceptablePositions, onAddPlayer, onRemovePlayer, onReplacePlayerOnField}: {
  positionShortName: string,
  name?: string | null,
  positionOnField: string | null,
  acceptablePositions?: string | string[],
  onAddPlayer: (playerName: string, position: string, positionOnField: string) => void;
  onRemovePlayer: (playerName: string, position: string, positionOnField: string) => void;
  onReplacePlayerOnField: (hoverName: string, dropName: string) => void;
  }) {
  // const [{ canDrop, isOver }, drop] = useDrop(() => ({
  const [monitor, drop] = useDrop(() => ({
    accept: positionShortName, // пока что захардкодим позицию, в последствии брать ее из пропса это будет принимаемая позиция
    drop: () => ({ position: positionShortName, positionOnField, name}),
    // пока что захардкодим позицию, в последствии брать ее из пропса
    collect: (monitor: any) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }));

  const isActive = monitor.canDrop && monitor.isOver
  let backgroundColor = '#222'
  if (isActive) {
    backgroundColor = 'darkgreen'
  } else if (monitor.canDrop) {
    backgroundColor = 'darkkhaki'
  }

  if (name) {
    return PlayerOnPosition(name, positionShortName, positionOnField, onReplacePlayerOnField, onRemovePlayer, onAddPlayer)
  }

  if (isActive) {
    return <div
      ref={drop}
      style={{ backgroundColor }}
      className={`position ${positionOnField}`}>
      Поставить ${name} на позицию ${positionOnField}
    </div>
  }

  return <div
    ref={drop}
    style={{ backgroundColor }}
    className={`position ${positionOnField}`}>
    {positionOnField}
  </div>
}

