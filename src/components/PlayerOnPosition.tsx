// @ts-nocheck
import React, {useRef} from 'react';
import {useDrag, useDrop} from 'react-dnd';

/**
 * Карточка игрока на поле
 * */
export const PlayerOnPosition = (name: any, position: any, positionOnField: any, onReplacePlayerOnField: any, onRemovePlayer: any, onAddPlayer: any) => {
  const ref = useRef(null);

  const [, drop] = useDrop({
    accept: position,
    // todo: ну и нахрена мне этот hover?
    hover(item, monitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.positionOnField;

      const hoverIndex = positionOnField;

      if (dragIndex === hoverIndex) {
        return
      }

      const hoveredRect = ref.current.getBoundingClientRect();
      const hoverMiddleY = (hoveredRect.bottom - hoveredRect.top) / 2;
      const mousePosition = monitor.getClientOffset();
      const hoverClientY = mousePosition.y - hoveredRect.top;

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      item.index = hoverIndex;
    },
    drop: (item, monitor) => {
      if (item.positionOnField) {
        onReplacePlayerOnField(positionOnField, item.positionOnField);
      } else {
        // что делаем с этими игроками?
        // а что это блин за игроки?
        // судя по тому что у него нет positionOnField - это хуй из листа
        // значит, возвращаем нижнего в лист, а тасканного хрена ставим на поле
        // console.log('что делаем с этими игроками?');
        // console.log('этого ублюдка мы тащим на поле?', item); // < это элемент который тащим, и тащим его на поле?
        console.log('а этого обратно в лист?', name, position, positionOnField); // это хрен который внизу, его обратно нахрен в список
        // console.log('ДА!');
        // onAddPlayer(item.name, item.positionOnField)
        onRemovePlayer({name: item.name, position, positionOnField});
        return;
      }
    }
  });

  const [{isDragging}, drag] = useDrag(() => ({
    type: position,
    item: {name, positionOnField},
    end: (item, monitor) => {
      const positionDrop = monitor.getDropResult<DropResult>();

      if (item && positionDrop && positionDrop.position) {
        onReplacePlayerOnField(positionDrop.positionOnField, item.positionOnField);
      } else {
        console.log('fuck that shit 2')
      }
    },
  }), []);

  drag(drop(ref));

  const opacity = isDragging ? 0.4 : 1;
  return <div ref={ref} style={{ opacity, backgroundColor: "red", position: "absolute", zIndex: 999 }}  className={`position ${positionOnField}`}>
    {name}: {positionOnField}
  </div>
}
