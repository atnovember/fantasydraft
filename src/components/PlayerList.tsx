// @ts-nocheck
import React, {useState} from 'react';
import {observer} from "mobx-react";
import {PlayerCard} from "./PlayerCard";
import {TeamPlayer} from "../store";

// todo: make it droppable! probably better make it on card!
export const PlayerList = observer(({store}: any) => {
  const [playerNameFilter, setPlayerNameFilter] = useState<string>('');

  const onAddPlayer = (player: string, position: string, positionOnField: string) => {
    console.log('onAddPlayer', player, position, positionOnField); // onAddPlayer Sisinio undefined undefined
    store.addPlayerToTeam(player, position, positionOnField);
  };



  const filterPlayers = (val: string) => {
    setPlayerNameFilter(val)
  }

  // store.listPlayers.forEach((player: TeamPlayer) => {
  //     console.log('listPlayers', player.name);
  // });

  return (
    <div className='player-list-wrapper'>
      <div>
        <input
          placeholder={'выберите игрока'}
          value={playerNameFilter}
          onChange={(e) => filterPlayers(e.target.value)}
        />
      </div>
      <br/>
      <div className='player-list'>
        {/* BOX ANALOG */}
        {store.listPlayers
          .map((player: TeamPlayer, index: number) => (
            <PlayerCard
              key={`${player.name}-${player.position}-${index}`}
              name={player.name}
              position={player.position}
              onAddPlayer={onAddPlayer}
              // onRemovePlayer={onRemovePlayer}
              // onReplacePlayerOnField={onReplacePlayerOnField}
            />

          ))}
      </div>
    </div>
  )

})
