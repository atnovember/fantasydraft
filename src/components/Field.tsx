// @ts-nocheck
import { observer } from "mobx-react";
import PlayerPosition from "./PlayerPosition";
import {TeamPlayer} from "../store";

export const Field = observer(({store}: any) => {

  const onAddPlayer = (player: TeamPlayer) => {
    console.log('onAddPlayer', player);
    store.addPlayerToTeam(player);
  };

  const onRemovePlayer = (player: TeamPlayer) => {
    console.log('onRemovePlayer >>> ', player);
    store.removePlayerFromTeam({...player});
  };

  const onReplacePlayerOnField = (hoverName: string, dropName: string) => {
    console.log('onReplacePlayerOnField', hoverName, dropName);
    store.replacePlayerOnField(hoverName, dropName);
  }

  store.teamPlayers.forEach((player: TeamPlayer) => {
    console.log('positionOnField', player?.positionOnField, player?.name);
  });

  return (
    <div className='field'>
      {
        store.teamPlayers
          .map((player: TeamPlayer) =>
            <PlayerPosition
              key={`${player.name}-${player.positionOnField}`}
              positionShortName={player.position}
              name={player.name}
              positionOnField={player.positionOnField}
              onAddPlayer={onAddPlayer}
              onRemovePlayer={onRemovePlayer}
              onReplacePlayerOnField={onReplacePlayerOnField}
            />
      )}

    </div>
  )
})
