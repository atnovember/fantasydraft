// @ts-nocheck
import React from 'react';
import {useDrag} from 'react-dnd';


export interface BoxProps {
  name: string | null;
  position: string;
  positionOnField?: string;
  onAddPlayer: (playerName: string, position: string, positionOnField: string) => void;
  onRemovePlayer: (playerName: string, position: string, positionOnField: string) => void;
}

interface DropResult {
  name: string | null,
  position: string;
  positionOnField: string;
}

/**
 * Карточка игрока в списке игроков
 * */
export const PlayerCard = ({name, position, positionOnField, onAddPlayer}: BoxProps) => {
  const [{isDragging}, dragRef] = useDrag(
    () => ({
      type: position,
      item: {name, positionOnField},
      end: (item, monitor) => {
        const positionDrop = monitor.getDropResult<DropResult>();

        if (item && positionDrop && positionDrop.position) {
          onAddPlayer(item.name || '', positionDrop.position, positionDrop.positionOnField);
        } else {
          console.log('fuck that shit')
        }
      },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
        handler: monitor.getHandlerId(),
      })
    }),
    []
  )

  const opacity = isDragging ? 0.4 : 1;

  return (
    <div ref={dragRef} style={{opacity}} className={'player'}>
      {name || ''} [{position}]
    </div>
  )
}
