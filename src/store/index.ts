import {action, makeObservable, observable} from "mobx";
import {playerList as listPlayers} from "../data/playerLIst";
import {teamPlayers} from '../data/teamPlayers';

export type TeamPlayer = {
  name: string | null;
  position: string;
  positionOnField: string | null;
}

class ObservableTeamPlayersStore {
  teamPlayers: TeamPlayer[] = teamPlayers;
  listPlayers: Omit<TeamPlayer, "positionOnField">[] = listPlayers;

  constructor() {
    makeObservable(this, {
      teamPlayers: observable,
      listPlayers: observable,
      addPlayerToTeam: action,
      replacePlayerOnField: action,
      removePlayerFromTeam: action,
    });
  }

  addPlayerToTeam(playerName: string, position: string, positionOnField: string) {
    const teamPlayerIndex = this.teamPlayers.findIndex(position => position.positionOnField === positionOnField);

    const exTeamPlayer = this.teamPlayers[teamPlayerIndex];
    this.teamPlayers[teamPlayerIndex] = {
      name: playerName,
      position: position,
      positionOnField: positionOnField
    };

    const players = this.listPlayers;
    const playerListIndex = players.findIndex((player) => player.name === playerName);
    if (exTeamPlayer.name !== null) {
      players[playerListIndex] = exTeamPlayer;
    } else {
      players.splice(playerListIndex, 1);
    }
  }

  // todo: это была плохая идея привязываться к именам игроков. надо будет сделать им поле id
  replacePlayerOnField(hoverPlayerPosOnField: string, droppablePlayerPosOnField: string) {
    const hoverPlayerIndex = this.teamPlayers.findIndex(position => position.positionOnField === hoverPlayerPosOnField);
    const dropPlayerIndex = this.teamPlayers.findIndex(position => position.positionOnField === droppablePlayerPosOnField);

    const hoverPlayer = {...this.teamPlayers[hoverPlayerIndex]};
    const dropPlayer = {...this.teamPlayers[dropPlayerIndex]};

    this.teamPlayers[dropPlayerIndex].name = hoverPlayer.name;
    this.teamPlayers[hoverPlayerIndex].name = dropPlayer.name;

  }

  removePlayerFromTeam(player: TeamPlayer) {
    const hoverPlayerIndex = this.listPlayers.findIndex(p => {
      return p.name === player.name;
    });

    const dropPlayerIndex = this.teamPlayers.findIndex(p => {
      return p.positionOnField === player.positionOnField
    });

    this.listPlayers[hoverPlayerIndex] = {
      name: this.teamPlayers[dropPlayerIndex].name,
      position: this.teamPlayers[dropPlayerIndex].position,
    }

    this.teamPlayers[dropPlayerIndex] = player
  }

  // get ?? todo: сделать подсчет игроков на поле и сделать подсчет игроков запаса

}

export const observableTeamPlayers = new ObservableTeamPlayersStore();
