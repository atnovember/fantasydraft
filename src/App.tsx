import React from 'react';
import './App.css';

import {DndProvider} from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import {PlayerList} from './components/PlayerList';
import {observer} from 'mobx-react';
import {Field} from './components/Field';

const App = observer(({store}: any) => {

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Расставьте игроков по позициям
        </p>
      </header>
      <main className={'choose-team'}>
        <DndProvider backend={HTML5Backend}>

          <div className={'left'}>
            {/* DUSTBIN ANALOG */}
            <Field store={store}/>
            <div className={'substitution-bench'}>
              <span style={{color: 'white'}}>Скамейка запасных:</span>
            </div>
          </div>


          <PlayerList store={store}/>
        </DndProvider>
      </main>

    </div>
  );
});

export default App;
